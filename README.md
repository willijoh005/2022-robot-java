Might not be 2022 Robot code IDK.
Swerve drive code for the 2022 season

Uses Swerve Drive Specialties's MK4 with L2 gear ratio with falcons as the motors.
(https://www.swervedrivespecialties.com/products/mk4-swerve-module?variant=39376675045489)

Electrical / Programming Wiring Chart.
(https://docs.google.com/spreadsheets/d/1KCYpCz1mNoaCfkUa-aLaaVEg1ivP0he2kCzS-OsVOjM/edit?usp=sharing)
It is restricted so only added people can open the sheets.

-- Add More Info When It Comes Up --
